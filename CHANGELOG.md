# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## 0.5.0
### Changed
- Allow to override default severity with TRIVY_SECURITY env var (Added)
- Trivy update

## 0.4.0
### Changed
- Versions (python, deps) upgrade

## 0.3.1
### Fixed
- Ensure to get all issues when querying gitlab
- Fix buggy reorder method

## 0.3.0
### Changed
- Sort issues

## 0.2.0
### Fixed
- Give time before removing volume
### Changed
- Team label for monitoring

## 0.1.4
### Fixed
- Do not use labels to determine if an issue should be created

## 0.1.3
### Fixed
- Fix team prefix

## 0.1.2
### Fixed
- Labels should be a list, a set is not serializable as json

