# Gitlab Vulnerabilities

## Purpose

The purpose of this script is to automate the analysis of docker images by [Trivy](https://aquasecurity.github.io/trivy/) and to manage the related GitLab issues.

## Workflow

A list of docker images to be analyzed is provided to the script; for each of these images:
- a Trivy analysis is run
- if no vulnerabilities are found, and no open GitLab issue exists for this image, then nothing is done
- if no vulnerabilities are found, and an open GitLab issue exists for this image, then that issue is closed
- if vulnerabilities are found, and no open GitLab issue exists for this image, one is created with the appropriate tags
- if vulnerabilities are found, and an open GitLab issue exists for this image, it is updated with the appropriate tags

## Usage

Once built, e.g. with `docker build -t gitlab_vulnerabilities -f _docker/Dockerfile .`, the Docker image can be used as such:
```bash
docker run --rm -t -v /var/run/docker.sock:/var/run/docker.sock:ro gitlab_vulnerabilities --gitlab-token $GITLAB_TOKEN alpine:latest python:latest
```
