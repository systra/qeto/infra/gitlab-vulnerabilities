#!/usr/bin/env python3
from argparse import ArgumentParser
from csv import DictReader
from dataclasses import dataclass
from datetime import datetime, timedelta, timezone
from json import JSONDecodeError, loads
from os import environ
from pathlib import Path
from re import Pattern, compile
from time import sleep
from typing import Any, Iterator, NamedTuple, TypeAlias, cast

from docker import from_env
from docker.errors import APIError
from gitlab import Gitlab
from gitlab.v4.objects.issues import ProjectIssue
from gitlab.v4.objects.projects import Project


class DryRun:
    def __init__(self, dry_run: bool):
        self.dry_run = dry_run

    def do_action(self, message: str) -> bool:
        print(f'[DRY RUN] {message}' if self.dry_run else message)
        return not self.dry_run
    print_message = do_action


class Analyzer(DryRun):
    """
    First use `preload`, then `analyze` on numerous images then `cleanup`,
    """
    DEFAULT_TRIVY_VERSION = '0.52.2'
    DOCKER_SOCKET = Path('/var/run/docker.sock')
    TRIVY_CACHE_VOLUME_NAME = 'trivy_cache'
    TRIVY_CACHE_MOUNT_DIR = '/root/.cache'
    DEFAULT_TRIVY_SEVERITY = 'HIGH,CRITICAL'
    ANALYSIS_COMMAND = 'trivy image --no-progress --scanners vuln --ignore-unfixed -f json'

    def __init__(self, dry_run: bool):
        super().__init__(dry_run)
        self.docker_client = from_env()
        self.trivy_version = environ.get('TRIVY_VERSION', self.DEFAULT_TRIVY_VERSION)
        self.trivy_severity = environ.get('TRIVY_SEVERITY', self.DEFAULT_TRIVY_SEVERITY)

    def preload(self) -> None:
        self.docker_client.volumes.create(name=self.TRIVY_CACHE_VOLUME_NAME)
        self.docker_client.containers.run(
            f'aquasec/trivy:{self.trivy_version}',
            'image --download-db-only',
            auto_remove=True,
            tty=True,
            volumes={
                self.TRIVY_CACHE_VOLUME_NAME: {'bind': self.TRIVY_CACHE_MOUNT_DIR, 'mode': 'rw'},
            },
        )

    def cleanup(self) -> None:
        MAX_TRIES = 3
        cur_try = MAX_TRIES
        last_error: Exception | None = None
        while cur_try:
            try:
                self.docker_client.volumes.get(self.TRIVY_CACHE_VOLUME_NAME).remove()
                break
            except APIError as e:
                last_error = e
                sleep(1)
                cur_try -= 1
        else:
            if last_error:
                raise last_error

    def analyze(self, image_id: str) -> dict[str, Any]:
        analysis_container = self.docker_client.containers.create(
            f'aquasec/trivy:{self.trivy_version}',
            f'sh -c "{self.ANALYSIS_COMMAND} -s {self.trivy_severity} -o output.json {image_id} >/dev/null 2>&1 && cat output.json"',
            tty=True,
            volumes={
                str(self.DOCKER_SOCKET): {'bind': str(self.DOCKER_SOCKET), 'mode': 'ro'},
                self.TRIVY_CACHE_VOLUME_NAME: {'bind': self.TRIVY_CACHE_MOUNT_DIR, 'mode': 'rw'},
            },
            entrypoint='',
        )
        try:
            analysis_container.start()
            analysis_container.wait()
            trivy_output = analysis_container.logs()
        finally:
            analysis_container.remove()
        try:
            return loads(trivy_output) if trivy_output else {}
        except JSONDecodeError:
            self.print_message("Error loading JSON:")
            self.print_message(trivy_output)
            raise


@dataclass
class CVEInfo:
    vulnerability_id: str
    primary_url: str
    title: str
    pkg_name: str
    pkg_path: str | None
    installed_version: str
    fixed_version: str

    def __str__(self):
        s = f"{self.title} [{self.vulnerability_id}]({self.primary_url})  \n{self.pkg_name}: `{self.installed_version}` → `{self.fixed_version}`"
        if self.pkg_path:
            s += f"  \npath: `{self.pkg_path}`"
        return s


CVEInfoDict: TypeAlias = dict[str, list[CVEInfo]]


@dataclass
class Stats:
    issues_created: int
    issues_closed: int


class IssueManager(DryRun):
    """
    Use it like:
    with IssueManager(dry_run, gitlab_token, security_project_id) as manager:
        manager.process(image1)
        manager.process(image2)
        ...
    """
    team_labels_path = Path(__file__).parent / 'team_labels.csv'

    def __init__(self, dry_run: bool, gitlab_token: str, security_project_id: int):
        super().__init__(dry_run)
        self.gitlab_token = gitlab_token
        self.security_project_id = security_project_id
        self.analyzer = Analyzer(dry_run)
        self.stats = Stats(0, 0)
        self.team_labels: dict[Pattern, str] = dict()
        with self.team_labels_path.open() as f:
            reader = DictReader(f, dialect='unix')
            for line in reader:
                self.team_labels[compile(line['Regex'])] = line['Label']

    @property
    def security_project(self) -> Project:
        if not hasattr(self, '_security_project'):
            gitlab_client = Gitlab(private_token=self.gitlab_token, per_page=1000)
            self._security_project = gitlab_client.projects.get(self.security_project_id)
        return self._security_project

    def __enter__(self) -> 'IssueManager':
        self.analyzer.preload()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.analyzer.cleanup()

    def process(self, image_id: str) -> None:
        self.print_message(f"Processing image {image_id}")
        analysis = self.analyzer.analyze(image_id)
        labels = self._get_labels(analysis, image_id)
        cve_infos = self._get_cve_infos(analysis)
        self._create_or_update_open_issue(image_id, labels, cve_infos)

    def _get_labels(self, analysis: dict[str, Any], image_id: str) -> set[str]:
        labels = set()
        for target in analysis.get('Results', []):
            # If this target has non-existing or empty vulnerabilites array, skip it
            if not target.get('Vulnerabilities'):
                continue
            if target['Class'] == 'os-pkgs':
                labels.add('OS')
            elif target['Type'] == 'python-pkg' or target['Type'] == 'pipenv':
                labels.add('Python')
            elif target['Type'] == 'gobinary':
                labels.add('Go')
            elif target['Type'] == 'yarn':
                labels.add('JS')
            elif target['Type'] == 'node-pkg':
                labels.add('npm')
            else:
                labels.add('Other')
        if (team_label := self._get_team_label(image_id)):
            labels.add(team_label)
        return labels

    def _get_team_label(self, image_id: str) -> str | None:
        for pattern, label in self.team_labels.items():
            if pattern.match(image_id):
                return label
        else:
            return None

    def _get_cve_infos(self, analysis: dict[str, Any]) -> CVEInfoDict:
        cves: CVEInfoDict = dict()
        for target in analysis.get('Results', []):
            vulnerabilities = target.get('Vulnerabilities')
            if not vulnerabilities:
                continue
            for vuln in vulnerabilities:
                kind = f"{target.get('Target', '')}, {target.get('Class', '')}, {target.get('Type', '')}"
                if kind not in cves:
                    cves[kind] = []
                cve_info = CVEInfo(
                    vulnerability_id=vuln['VulnerabilityID'],
                    primary_url=vuln.get('PrimaryURL', ''),
                    title=vuln.get('Title', vuln.get('Description', vuln['VulnerabilityID'])[:20]),
                    pkg_name=vuln['PkgName'],
                    pkg_path=vuln.get('PkgPath'),
                    installed_version=vuln['InstalledVersion'],
                    fixed_version=vuln['FixedVersion'],
                )
                if cve_info not in cves[kind]:
                    cves[kind].append(cve_info)
        return cves

    def _get_kind_and_cve_generator(self, kind: str, cve_infos: list[CVEInfo]) -> Iterator[str]:
        yield f'**{kind}**'
        for cve_info in cve_infos:
            yield str(cve_info)

    def _create_or_update_open_issue(self, image_id: str, labels: set[str], cve_infos: CVEInfoDict) -> None:
        """
        Search for existing issues on this image, in 'Open' status
          - if it doesn't exist, create it
          - if it exists, update labels
        """
        open_issues = self._get_open_issues(image_id)
        issue: ProjectIssue | None = open_issues[0] if open_issues else None
        if issue:
            self.print_message(f"  Updating issue {issue.iid}")
        elif cve_infos:
            if self.do_action(f"  Creating an issue for image {image_id}"):
                issue = cast(ProjectIssue, self.security_project.issues.create({'title': image_id}))
            self.stats.issues_created += 1
        else:
            self.print_message("  No vulnerabilities and no open issue, nothing to do")
            return
        if issue:
            is_issue_needs_to_be_saved = False
            existing_labels = {label for label in issue.labels if not label.startswith('status::')}
            if not {label for label in labels if '::' not in label}:
                if self.do_action("  No vulnerabilities for this image, closing issue"):
                    issue.state_event = 'close'
                    issue.labels = ['status::done']
                    is_issue_needs_to_be_saved = True
                    self.stats.issues_closed += 1
            elif existing_labels == labels:
                self.print_message(f"  The issue's labels are correctly set to {labels}, no update")
            elif self.do_action(f"  Setting its labels to {labels}"):
                issue.labels = list({label for label in issue.labels if label.startswith('status::')} | labels)
                is_issue_needs_to_be_saved = True
            if cve_infos:
                num_cves = sum(len(cves) for cves in cve_infos.values())
                self.print_message(f"  The issue's description will be set to {num_cves} CVEs")
                issue.description = '\n\n---\n\n'.join('\n\n'.join(self._get_kind_and_cve_generator(kind, cves)) for kind, cves in cve_infos.items())
                is_issue_needs_to_be_saved = True
            if is_issue_needs_to_be_saved:
                issue.save()

    def _get_open_issues(self, image_id: str) -> list[ProjectIssue]:
        # https://docs.gitlab.com/ee/api/issues.html#list-issues
        return cast(list[ProjectIssue], self.security_project.issues.list(
            get_all=True,
            scope='all',
            search=image_id,
            state='opened',
            **{'not[labels]': 'status::done'},
        ))

    def show_stats(self):
        self.print_message(f"Statistics: {self.stats.issues_created} issues created, {self.stats.issues_closed} issues closed")

    def sort_issues(self):
        for selection in ({'not[labels]': ['status::doing', 'status::done']}, {'labels': 'status::doing'}):
            issues = cast(list[ProjectIssue], self.security_project.issues.list(
                all=True,
                state='opened',
                **selection,
            ))
            nb = len(issues)
            if nb > 1 and self.do_action(f"Sorting {nb} issues matching {selection}"):
                sorted_issues = sorted(issues, key=lambda issue: issue.title)
                for i, (issue1, issue2) in enumerate(zip(sorted_issues[:-1], sorted_issues[1:]), 1):
                    self.print_message(f"  {i}/{nb}")
                    # https://docs.gitlab.com/ee/api/issues.html#reorder-an-issue
                    # reorder method is buggy, custom request should be made
                    issue1.manager.gitlab.http_put(
                        f"{issue1.manager.path}/{issue1.encoded_id}/reorder",
                        query_data={'move_before_id': issue2.id},
                    )
                    # issue1.reorder(move_before_id=issue2.id)

    def close_old_issues(self):
        if self.do_action("Closing old done issues"):
            today_minus_one_day = datetime.now(timezone.utc) - timedelta(days=1)
            # https://docs.gitlab.com/ee/api/issues.html#list-issues
            issues = cast(list[ProjectIssue], self.security_project.issues.list(
                all=True,
                state='opened',
                labels='status::done',
                updated_before=today_minus_one_day.isoformat(timespec='seconds'),
            ))
            for issue in issues:
                issue.state_event = 'close'
                issue.save()


class ParsedArgs(NamedTuple):
    gitlab_token: str
    security_project_id: int
    dry_run: bool
    image_ids: set[str]


def parse_arguments() -> ParsedArgs:
    parser = ArgumentParser()
    parser.add_argument(
        '-t', '--gitlab-token',
        required=True,
    )
    parser.add_argument(
        '-p', '--security-project-id',
        type=int,
        default=30549504,  # https://gitlab.com/systra/qeto/infra/security
    )
    parser.add_argument(
        '-d', '--dry-run',
        action='store_true',
        default=False,
    )
    parser.add_argument(
        dest='image_ids',
        metavar='IMAGE_ID',
        nargs='+',
        help='Space-separated list of Docker images',
    )
    args = parser.parse_args()
    args.image_ids = set(args.image_ids)
    return cast(ParsedArgs, args)


def main(args: ParsedArgs) -> None:
    with IssueManager(args.dry_run, args.gitlab_token, args.security_project_id) as manager:
        for image_id in sorted(args.image_ids):
            manager.process(image_id)
        manager.show_stats()
        manager.sort_issues()
        manager.close_old_issues()


if __name__ == '__main__':
    if not Analyzer.DOCKER_SOCKET.exists():
        raise FileNotFoundError(f"{Analyzer.DOCKER_SOCKET} should be mounted to run this script")
    main(parse_arguments())
